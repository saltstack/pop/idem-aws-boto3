from typing import Any, Dict
from asyncio import Task

# Ensure that all user states are run before running key-based states
TREQ = {
    "present": {"require": ["aws.iam.user.present"], "soft_require": ["aws.iam.user.absent"],},
    "absent": {"require": ["aws.iam.user.present"], "soft_require": ["aws.iam.user.absent"],},
}


async def state(hub, ctx, **kwargs):
    """
    List all ssh keys and get their AWS 'ids':
    """
    username: str = kwargs["username"]
    if "key" not in kwargs:
        print(kwargs)
    key = _normalize_ssh_key(kwargs["key"])

    success, ret = await hub.exec.aws.iam.list_ssh_public_keys(ctx, username=username)
    if success is not True:
        return success, ret

    key_reverse_mappings = {}

    # Grab extended data for all SSH keys, all at once:
    # TODO this is the slowest part when it happens one key at a time, can we get just the key_id for the key we are interracting with?

    grabber_tasks = []
    ssh_key_ids = set(map(lambda x: x["SSHPublicKeyId"], ret))

    for key_id in ssh_key_ids:
        grabber_tasks.append(Task(hub.exec.aws.iam.get_ssh_public_key(ctx, username=username, key_id=key_id)))

    async for success, result in hub.tool.aws.common.gather_pending_tasks(grabber_tasks):
        if not success:
            return False, result
        key_obj = result["SSHPublicKey"]
        key_reverse_mappings[key_obj["SSHPublicKeyBody"]] = key_obj
        if key_obj["SSHPublicKeyBody"] == key:
            return True, key_obj

    return True, {}


def _normalize_ssh_key(key: str) -> str:
    # This removes the trailing email/identifier string from the end of the key if present. AWS
    # does not store this, so for string comparisons, we must remove it from ours too:
    key = key.strip()
    key_split = key.split()
    key = " ".join(key_split[:2])
    return key


async def present(hub, ctx, **kwargs) -> Dict[str, Any]:
    """
    Ensure the specified ssh key exists for this username, and is active.

    `username` is self explanatory. `key` should be the literal `ssh-rsa asdf;laskdf` keydata
    in OpenSSH format.

    This function works as follows. It will list all the ssh keys for a user, get key IDs, then
    query extended information for each of these IDs, so we have the literal SSH key data client-
    side. Then we will make sure that `key` exists and is active.

    If it is, great. If not active, we use the specific API call to make it active. If it doesn't
    exist, we will upload the key.

    Returns a tuple -- a boolean indicating success or failure, and a string indicating what
    action was taken.

    IMPORTANT NOTE: This function is not fast. But it is correct and complete, and leverages
    several exec functions which is good for testing. A future version should do client-side
    caching for SSH key data so it doesn't have to keep re-querying AWS for this with each call.
    """
    # Get the arguments for the state
    username: str = kwargs["username"]
    key = _normalize_ssh_key(kwargs["key"])
    status: str = kwargs.get("status", "Active").title()
    key_state = ctx.before.copy()

    # Find out if our SSH key exists

    if not ctx.before:
        hub.log.debug(f"Key {key} doesn't exist, upload it")

        if ctx.test:
            ctx.ret.result = True
            ctx.ret.comment = "Key would be uploaded"
            return ctx.ret

        ctx.ret.result, result = await hub.exec.aws.iam.upload_ssh_public_key(ctx, username=username, key=key)

        if not ctx.ret.result:
            # Key upload failure, return immediately
            ctx.ret.comment = result
            return ctx.ret

        ctx.ret.comment = "Key uploaded."
        success, key_state = await hub.states.aws.iam.ssh_key.state(ctx, username=username, key=key)

    # Now that the key exists for sure, verify that it is in the right state
    if key_state["Status"] == status:
        hub.log.debug(f"Key {key} is in the correct state")

        ctx.ret.result = True
        ctx.ret.comment += f"\nKey ID {key} already exists in correct state."
        return ctx.ret

    hub.log.debug(f"Key {key} is {key_state['Status']}, setting it to {status}")
    if ctx.test:
        ctx.ret.result = True
        ctx.ret.comment += f"Key ID {key} would be set to {status} status"
        return ctx.ret

    ctx.ret.result, result = await hub.exec.aws.iam.update_ssh_public_key(ctx, username=username, key_id=key_state["SSHPublicKeyId"], status=status,)
    if ctx.ret.result:
        ctx.ret.comment += f"\nKey ID {key} set to {status} status."
    else:
        ctx.ret.comment = result

    return ctx.ret


async def absent(hub, ctx, **kwargs) -> Dict[str, Any]:
    """
    Ensure the specified ssh key does not exist for this username
    """
    # Get the arguments for the state
    username: str = kwargs["username"]
    key = _normalize_ssh_key(kwargs["key"])

    # Find our SSH key:

    if key in ctx.before:
        key_id = ctx.before[key]["SSHPublicKeyId"]
        if ctx.test:
            ctx.ret.comment = "Key would be deleted"
        else:
            # Key exists, remove it
            ctx.ret.result, result = await hub.exec.aws.iam.delete_ssh_public_key(ctx, username=username, key_id=key_id)
            if ctx.ret.result:
                ctx.ret.comment = "Key deleted."
            else:
                ctx.ret.comment = result
    else:
        ctx.ret.result = True
        ctx.ret.comment = f"Key {key} is already absent"
    return ctx.ret
