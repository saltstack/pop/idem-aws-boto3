# See this file for details of structure of return values, etc.
#
# https://botocore.amazonaws.com/v1/documentation/api/latest/reference/services/iam.html

# Main TODO list. There are sub-TODO lists in some sections below, too.
# add_client_id_to_open_id_connect_provider()
# can_paginate()
# create_open_id_connect_provider()
# create_policy()
# create_policy_version()
# create_saml_provider()
# create_service_linked_role()
# create_service_specific_credential()
# create_virtual_mfa_device()
# deactivate_mfa_device()
# delete_access_key()
# delete_account_alias()
# delete_account_password_policy()

# delete_open_id_connect_provider()
# delete_policy()
# delete_policy_version()
# delete_saml_provider()
# delete_server_certificate()
# delete_service_specific_credential()
# delete_signing_certificate()
# delete_user_permissions_boundary()
# delete_virtual_mfa_device()
# enable_mfa_device()
# generate_credential_report()
# generate_organizations_access_report()
# generate_presigned_url()
# generate_service_last_accessed_details()
# get_account_authorization_details()
# get_account_password_policy()
# get_account_summary()
# get_context_keys_for_custom_policy()
# get_context_keys_for_principal_policy()
# get_credential_report()

# get_open_id_connect_provider()
# get_organizations_access_report()
# get_paginator()
# get_policy()
# get_policy_version()
# get_saml_provider()
# get_server_certificate()
# get_service_last_accessed_details()
# get_service_last_accessed_details_with_entities()
# get_waiter()
# list_virtual_mfa_devices()
# remove_client_id_from_open_id_connect_provider()
# reset_service_specific_credential()
# resync_mfa_device()
# set_default_policy_version()
# set_security_token_service_preferences()
# simulate_custom_policy()
# simulate_principal_policy()
# update_account_password_policy()
# update_assume_role_policy()
# update_open_id_connect_provider_thumbprint()
# update_saml_provider()
# update_server_certificate()
# update_service_specific_credential()
# update_signing_certificate()
# upload_server_certificate()
# upload_signing_certificate()


ALL_LIST_DEFS = {}
ALL_CALL_TEMPLATES = {}

#####################################################################################################
# List: Base Objects: We will dynamically create methods for these below.
#####################################################################################################


def _wrap_list_method_with_path_prefix(hub, api_call, result_element):
    async def wrapper(ctx, path_prefix: str = "/"):
        return await hub.tool.aws.common.list_paginated(ctx, "iam", api_call, result_element, PathPrefix=path_prefix)

    return wrapper


ALL_LIST_DEFS["WITH_PATH_PREFIX"] = (
    _wrap_list_method_with_path_prefix,
    {
        "list_groups": "Groups",
        "list_users": "Users",
        "list_roles": "Roles",
        "list_instance_profiles": "InstanceProfiles",
        "list_instance_profiles_for_profile": "InstanceProfiles",
        "list_server_certificates": "ServerCertificateMetadataList",
    },
)


def _wrap_list_method_without_path_prefix(hub, api_call, result_element):
    async def wrapper(ctx):
        return await hub.tool.aws.common.list_paginated(ctx, "iam", api_call, result_element)

    return wrapper


ALL_LIST_DEFS["WITHOUT_PATH_PREFIX"] = (
    _wrap_list_method_without_path_prefix,
    {"list_account_aliases": "AccountAliases", "list_saml_providers": "SAMLProviderList", "list_open_id_connect_providers": "OpenIDConnectProviderList",},
)

#####################################################################################################
# List: User Attributes: We will dynamically create methods for these below.
#####################################################################################################


def _wrap_user_attribute_list_method(hub, api_call, result_element):
    async def wrapper(ctx, username: str = None):
        return await hub.tool.aws.common.list_paginated(ctx, "iam", api_call, result_element, UserName=username)

    return wrapper


ALL_LIST_DEFS["USER_ATTRIBUTES"] = (
    _wrap_user_attribute_list_method,
    {
        "list_access_keys": "AccessKeyMetadata",
        "list_account_aliases": "AccountAlias",
        "list_groups_for_user": "Groups",
        "list_ssh_public_keys": "SSHPublicKeys",
        "list_user_tags": "Tags",
        "list_user_policies": "PolicyNames",
        "list_mfa_devices": "MFADevices",
        "list_signing_certificates": "Certificates",
    },
)

#####################################################################################################
# Wrapper Functions -- Used at end of code to dynamically generate wrappers.
#####################################################################################################


def _aws_tagdict(tags):
    """
    Converts a Python dictionary into a format that AWS expects for tags.
    """
    aws_tags = []
    for key, val in tags:
        aws_tags.append({"Key": key, "Value": val})
    return aws_tags


# TODO:
# Is user-specific:
#  list_service_specific_credentials() # user and service


#####################################################################################################
# List: Policies
#####################################################################################################


# Has extra arguments related to policies:
#  list_policies()
#  list_policies_granting_service_access()
#  list_policy_versions()
# Has more data:
#  list_entities_for_policy()

#####################################################################################################
# "Current User" Methods. Methods which operate on the user currently authenticating.
#####################################################################################################
# change_password()
# create_account_alias()

#####################################################################################################
# User: Base Methods and User Policies
#####################################################################################################

# TODO:
#  get_user_policy()
#  list_attached_user_policies()
#  detach_user_policy()
#  delete_user_policy()
#  attach_user_policy()
#  put_user_permissions_boundary()
#  put_user_policy()

ALL_CALL_TEMPLATES["BASE_USER"] = {
    "create_user": (
        ["path", "permissions_boundary", "tags", "username"],
        {"optional": {"path", "permissions_boundary", "tags"}, "adapt": {"tags": _aws_tagdict}},
    ),
    "delete_user": ["username"],
    "get_user": ["username"],
    "update_user": (["username", "new_path", "new_username"], {"optional": {"new_path", "new_username"}}),
    "tag_user": (["username", "tags"], {"adapt": {"tags": _aws_tagdict}}),
    "untag_user": ["username", "tag_keys"],
}

#####################################################################################################
# User Login Profile -- Log in via AWS Console
#####################################################################################################

# Create/Update an AWS console login profile, so the specified user can log in with the supplied password.

ALL_CALL_TEMPLATES["LOGIN_PROFILE"] = {
    "create_login_profile": (["username", "password", "password_reset_required"], {"optional": {"password_reset_required"}}),
    "update_login_profile": (["username", "password", "password_reset_required"], {"optional": {"password_reset_required"}}),
    "get_login_profile": ["username"],
}

#####################################################################################################
# User Attribute: Access Keys
#####################################################################################################

ALL_CALL_TEMPLATES["ACCESS_KEY"] = {
    "create_access_key": ["username"],
    "update_access_key": (["username", "access_key_id", "status"], {"adapt": {"status": lambda x: x.capitalize()}}),
    "get_access_key_last_used": ["access_key_id"],
}

#####################################################################################################
# User Attribute: SSH Public Keys
#####################################################################################################

ALL_CALL_TEMPLATES["SSH_PUB_KEY"] = {
    "delete_ssh_public_key": ["username", "ssh_public_key_id"],
    "get_ssh_public_key": (["username", "ssh_public_key_id", "encoding"], {"adapt": {"encoding": lambda x: x.upper()}}),
    "upload_ssh_public_key": ["username", "ssh_public_key_body"],
    "update_ssh_public_key": (["username", "ssh_public_key_id", "status"], {"adapt": {"status": lambda x: x.capitalize()}}),
}

#####################################################################################################
# Group Policies and Attributes
#####################################################################################################

ALL_CALL_TEMPLATES["GROUP_ATTR"] = {
    "attach_group_policy": ["group_name", "policy_arn"],
    "delete_group_policy": ["group_name", "policy_name"],
    "detach_group_policy": ["group_name", "policy_arn"],
    "get_group_policy": ["group_name", "policy_name"],
    "put_group_policy": ["group_name", "policy_name", "policy_document"],
}


async def list_group_policies(hub, ctx, groupname=None):
    call_args = {"GroupName": groupname}
    return await hub.tool.aws.common.list_paginated(ctx, "iam", "list_group_policies", "PolicyNames", **call_args)


#####################################################################################################
# Roles and Role Policies
#####################################################################################################

ALL_CALL_TEMPLATES["ROLE"] = {
    "attach_role_policy": ["role_name", "policy_arn"],
    "delete_role_policy": ["role_name", "policy_name"],
    "delete_role": ["role_name"],
    "delete_role_permissions_boundary": ["role_name"],
    "delete_service_linked_role": ["role_name"],
    "detach_role_policy": ["role_name", "policy_arn"],
    "get_service_linked_role_deletion_status": ["deletion_task_id"],
    "create_role": (
        ["role_name", "path", "description", "max_session_duratiom", "tags", "assume_role_policy_document", "permissions_boundary"],
        {"optional": ["path", "description", "max_session_duration", "permissions_boundary", "tags"], "adapt": {"tags": _aws_tagdict}},
    ),
    "get_role": ["role_name"],
    "get_role_policy": ["role_name", "policy_name"],
    "put_role_permissions_boundary": ["role_name", "permissions_boundary"],
    "put_role_policy": ["role_name", "policy_name", "policy_document"],
    "update_role": (["role_name", "description", "max_session_duration"], {"optional": {"description", "max_session_duration"}}),
    "tag_role": (["role_name", "tags"], {"adapt": {"tags": _aws_tagdict}}),
    "untag_role": ["role_name", "tag_keys"],
}

# These all should take role_name (ie. "RoleName" on AWS side) as arguments:


def _wrap_role_policy_method(hub, api_call, result_element):
    async def wrapper(ctx, role_name: str = None):
        return await hub.tool.aws.common.list_paginated(ctx, "iam", api_call, result_element, RoleName=role_name)

    return wrapper


ALL_LIST_DEFS["ROLE_POLICY"] = (
    _wrap_role_policy_method,
    {"list_attached_role_policies": "AttachedPolicies", "list_role_policies": "PolicyNames", "list_role_tags": "Tags",},
)


#####################################################################################################
# Groups
#####################################################################################################


async def get_group(hub, ctx, groupname=None):
    """
    This function returns a list of Users in the specified group.
    """
    call_args = {
        "GroupName": groupname,
    }
    return await hub.tool.aws.common.list_paginated(ctx, "iam", "get_group", "Users", **call_args)


ALL_CALL_TEMPLATES["GROUP"] = {
    "add_user_to_group": ["username", "group_name"],
    "remove_user_from_group": ["username", "group_name"],
    "delete_group": ["group_name"],
    "create_group": ["group_name"],
    "update_group": (["group_name"], {"optional": ["new_path", "new_group_name"]}),
}


async def list_attached_group_policies(hub, ctx, groupname=None):
    call_args = {"GroupName": groupname}
    return await hub.tool.aws.common.list_paginated(ctx, "iam", "list_attached_group_policies", "AttachedPolicies", **call_args)


#####################################################################################################
# Instance Profiles
#####################################################################################################

ALL_CALL_TEMPLATES["INSTANCE_PROFILE"] = {
    "create_instance_profile": (["instance_profile_name"], {"optional": ["path"]}),
    "delete_instance_profile": ["instance_profile_name"],
    "get_instance_profile": ["instance_profile_name"],
    "add_role_to_instance_profile": ["instance_profile_name", "role_name"],
    "remove_role_from_instance_profile": ["instance_profile_name", "role_name"],
}

#####################################################################################################
# Dynamically Wrap Functions
#####################################################################################################


def __func_alias__(hub):
    out = {}

    # Wrap all listing-style functions:

    for func_type, func_data in ALL_LIST_DEFS.items():
        list_wrapper_fn, list_specs = func_data
        for api_call, result_element in list_specs.items():
            out[api_call] = list_wrapper_fn(hub, api_call, result_element)

    # Wrap all simple API call-style functions, using a function that understands our signatures:

    for func_type, methods in ALL_CALL_TEMPLATES.items():
        hub.tool.aws.wrapper.wrap_engine("iam", methods, out)

    return out
